package com.epam.rd.java.basic.task8.controller.sax;

import com.epam.rd.java.basic.task8.model.Employee;
import com.epam.rd.java.basic.task8.model.WorkingInfo;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.*;
import java.util.Comparator;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private final String xmlFileName;
	private List<Employee> employees;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE

	public void parse() {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParserHandler handler = new SAXParserHandler();
		SAXParser parser = null;
		File file = new File(xmlFileName);

		try {
			parser = factory.newSAXParser();
			parser.parse(file, handler);
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
			return;
		}

		employees = handler.getResult();
	}

	public void sort() {
		employees.sort(Comparator.comparing(o -> o.getWorkingInfo().getSalary()));
	}

	public void write(String path) {
		XMLOutputFactory factory = XMLOutputFactory.newFactory();
		XMLStreamWriter writer = null;

		try {
			 writer = factory.createXMLStreamWriter(new FileOutputStream(path));
			 generateXML(writer, employees);
		} catch (XMLStreamException | FileNotFoundException e) {
			e.printStackTrace();
		}

	}

	private static void generateXML(XMLStreamWriter writer, List<Employee> employees) throws XMLStreamException {
		writer.writeStartDocument();

		writer.writeStartElement("employees");
		writer.writeAttribute("xmlns", "http://www.test.ua");
		writer.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		writer.writeAttribute("xsi:schemaLocation", "http://www.test.ua");

		for (Employee employee : employees) {
			setEmployee(writer, employee);
		}

		writer.writeEndElement();
		writer.writeEndDocument();
	}

	private static void setEmployee(XMLStreamWriter writer, Employee employee) throws XMLStreamException {
		writer.writeStartElement("employee");

		writer.writeStartElement("name");
		writer.writeCharacters(employee.getName());
		writer.writeEndElement();

		writer.writeStartElement("surname");
		writer.writeCharacters(employee.getSurname());
		writer.writeEndElement();

		writer.writeStartElement("email");
		writer.writeCharacters(employee.getEmail());
		writer.writeEndElement();

		setWorkingInfo(writer, employee.getWorkingInfo());

		writer.writeEndElement();
	}

	private static void setWorkingInfo(XMLStreamWriter writer, WorkingInfo workingInfo) throws XMLStreamException {
		writer.writeStartElement("workingInfo");

		writer.writeStartElement("projectName");
		writer.writeCharacters(workingInfo.getProjectName());
		writer.writeEndElement();

		writer.writeStartElement("position");
		writer.writeCharacters(workingInfo.getPosition());
		writer.writeEndElement();

		writer.writeStartElement("salary");
		writer.writeAttribute("currency", "USD");
		writer.writeCharacters(String.valueOf(workingInfo.getSalary()));
		writer.writeEndElement();

		writer.writeEndElement();
	}


}