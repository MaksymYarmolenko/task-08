package com.epam.rd.java.basic.task8.controller.sax;

import com.epam.rd.java.basic.task8.model.Employee;
import com.epam.rd.java.basic.task8.model.WorkingInfo;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class SAXParserHandler extends DefaultHandler {

    private final StringBuilder currentValue;
    private final List<Employee> result;
    private Employee currentEmployee;
    private WorkingInfo currentWorkingInfo;
    private boolean insideWorkingInfo;

    public List<Employee> getResult() {
        return result;
    }

    public SAXParserHandler() {
        result = new ArrayList<>();
        currentValue = new StringBuilder();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        currentValue.setLength(0);

        if (qName.equalsIgnoreCase("employee")) {
            currentEmployee = new Employee();
            currentWorkingInfo = new WorkingInfo();
        }

        if (qName.equalsIgnoreCase("workingInfo")) {
            insideWorkingInfo = true;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase("name")) {
            currentEmployee.setName(currentValue.toString());
        }
        if (qName.equalsIgnoreCase("surname")) {
            currentEmployee.setSurname(currentValue.toString());
        }
        if (qName.equalsIgnoreCase("email")) {
            currentEmployee.setEmail(currentValue.toString());
        }
        if (insideWorkingInfo && qName.equalsIgnoreCase("projectName")) {
            currentWorkingInfo.setProjectName(currentValue.toString());
        }
        if (insideWorkingInfo && qName.equalsIgnoreCase("position")) {
            currentWorkingInfo.setPosition(currentValue.toString());
        }
        if (insideWorkingInfo && qName.equalsIgnoreCase("salary")) {
            currentWorkingInfo.setSalary(Integer.parseInt(currentValue.toString()));
        }

        if (qName.equalsIgnoreCase("employee")) {
            insideWorkingInfo = false;
            currentEmployee.setWorkingInfo(currentWorkingInfo);
            result.add(currentEmployee);
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        currentValue.append(ch, start, length);
    }
}
