package com.epam.rd.java.basic.task8.model;

public class Employee {
    private String name;
    private String surname;
    private String email;

    private WorkingInfo workingInfo;

    public Employee() {

    }

    public Employee(String name, String surname, String email, WorkingInfo workingInfo) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.workingInfo = workingInfo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public WorkingInfo getWorkingInfo() {
        return workingInfo;
    }

    public void setWorkingInfo(WorkingInfo workingInfo) {
        this.workingInfo = workingInfo;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", email='" + email + '\'' +
                ", workingInfo=" + workingInfo +
                '}';
    }
}
