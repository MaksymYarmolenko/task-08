package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.model.Employee;
import com.epam.rd.java.basic.task8.model.WorkingInfo;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.stream.*;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private final String xmlFileName;
	private final List<Employee> employees;
	private Employee currentEmployee;
	private WorkingInfo currentWorkingInfo;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
		employees = new ArrayList<>();
	}

	// PLACE YOUR CODE HERE

	public List<Employee> getEmployees() {
		return employees;
	}

	public void parse() {
		XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
		XMLEventReader reader = null;

		try {
			reader = xmlInputFactory.createXMLEventReader(new FileInputStream(xmlFileName));

			while (reader.hasNext()) {
				XMLEvent nextEvent = reader.nextEvent();
				getEmployees(reader);
			}

		} catch (XMLStreamException | FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void sort() {employees.sort(Comparator.comparing(o -> o.getWorkingInfo().getPosition()));}

	public void write(String path) {
		try (FileOutputStream fos = new FileOutputStream(path)) {
			writeXML(fos);
		} catch (IOException | XMLStreamException e) {
			e.printStackTrace();
		}
	}

	private void writeXML(FileOutputStream fos) throws XMLStreamException {
		XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
		XMLEventFactory eventFactory = XMLEventFactory.newInstance();

		XMLEventWriter writer = outputFactory.createXMLEventWriter(fos);

		writer.add(eventFactory.createStartDocument("utf-8", "1.0"));

		writer.add(eventFactory.createStartElement("", "", "employees"));
		writer.add(eventFactory.createAttribute("xmlns", "http://www.test.ua"));
		writer.add(eventFactory.createAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance"));
		writer.add(eventFactory.createAttribute("xsi:schemaLocation", "http://www.test.ua"));

		for (Employee employee : employees) {
			setEmployee(writer, eventFactory, employee);
		}

		writer.add(eventFactory.createEndElement("", "", "employees"));

		writer.add(eventFactory.createEndDocument());
		writer.flush();
		writer.close();
	}

	private void setEmployee(XMLEventWriter writer, XMLEventFactory eventFactory, Employee employee) throws XMLStreamException {
		writer.add(eventFactory.createStartElement("", "", "employee"));

		writer.add(eventFactory.createStartElement("", "", "name"));
		writer.add(eventFactory.createCharacters(employee.getName()));
		writer.add(eventFactory.createEndElement("", "", "name"));

		writer.add(eventFactory.createStartElement("", "", "surname"));
		writer.add(eventFactory.createCharacters(employee.getSurname()));
		writer.add(eventFactory.createEndElement("", "", "surname"));

		writer.add(eventFactory.createStartElement("", "", "email"));
		writer.add(eventFactory.createCharacters(employee.getEmail()));
		writer.add(eventFactory.createEndElement("", "", "email"));

		setWorkingInfo(writer, eventFactory, employee.getWorkingInfo());

		writer.add(eventFactory.createEndElement("", "", "employee"));
	}

	private void setWorkingInfo(XMLEventWriter writer, XMLEventFactory eventFactory, WorkingInfo workingInfo) throws XMLStreamException {
		writer.add(eventFactory.createStartElement("", "", "workingInfo"));

		writer.add(eventFactory.createStartElement("", "", "projectName"));
		writer.add(eventFactory.createCharacters(workingInfo.getProjectName()));
		writer.add(eventFactory.createEndElement("", "", "projectName"));

		writer.add(eventFactory.createStartElement("", "", "position"));
		writer.add(eventFactory.createCharacters(workingInfo.getPosition()));
		writer.add(eventFactory.createEndElement("", "", "position"));

		writer.add(eventFactory.createStartElement("", "", "salary"));
		writer.add(eventFactory.createAttribute("currency", "USD"));
		writer.add(eventFactory.createCharacters(String.valueOf(workingInfo.getSalary())));
		writer.add(eventFactory.createEndElement("", "", "salary"));

		writer.add(eventFactory.createEndElement("", "", "workingInfo"));
	}

	private void getEmployees(XMLEventReader reader) throws XMLStreamException {
		while (reader.hasNext()) {
			XMLEvent nextEvent = reader.nextEvent();
			if (nextEvent.isStartElement()) {
				StartElement startElement = nextEvent.asStartElement();

				switch (startElement.getName().getLocalPart()) {
					case "employee":
						currentEmployee = new Employee();
						break;
					case "name":
						nextEvent = reader.nextEvent();
						currentEmployee.setName(nextEvent.asCharacters().getData());
						break;
					case "surname":
						nextEvent = reader.nextEvent();
						currentEmployee.setSurname(nextEvent.asCharacters().getData());
						break;
					case "email":
						nextEvent = reader.nextEvent();
						currentEmployee.setEmail(nextEvent.asCharacters().getData());
						break;
					case "workingInfo":
						nextEvent = reader.nextEvent();
						currentWorkingInfo = new WorkingInfo();
						break;
					case "projectName":
						nextEvent = reader.nextEvent();
						currentWorkingInfo.setProjectName(nextEvent.asCharacters().getData());
						break;
					case "position":
						nextEvent = reader.nextEvent();
						currentWorkingInfo.setPosition(nextEvent.asCharacters().getData());
						break;
					case "salary":
						nextEvent = reader.nextEvent();
						currentWorkingInfo.setSalary(Integer.parseInt(nextEvent.asCharacters().getData()));
						break;
				}
			}
			if (nextEvent.isEndElement()) {
				EndElement endElement = nextEvent.asEndElement();
				if (endElement.getName().getLocalPart().equals("employee")) {
					currentEmployee.setWorkingInfo(currentWorkingInfo);
					employees.add(currentEmployee);
				}
			}
		}
	}
}