package com.epam.rd.java.basic.task8.controller.dom;

import com.epam.rd.java.basic.task8.model.Employee;
import com.epam.rd.java.basic.task8.model.WorkingInfo;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {
	private final String xmlFileName;
	private final List<Employee> employees;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
		employees = new ArrayList<>();
	}

	// PLACE YOUR CODE HERE

	public void parse() {
		Document document = parseDocument(xmlFileName);
		NodeList employeesNodeList = document.getFirstChild().getChildNodes();
		parseEmployees(employeesNodeList);
	}

	public void sort() {
		employees.sort((Comparator.comparing(Employee::getName)));
	}

	public void write(String path) {
		new DOMWriter(path, employees).configureXmlFile();
	}

	private void parseEmployees(NodeList employeesNodeList) {
		for (int i = 0; i < employeesNodeList.getLength(); i++) {
			if(employeesNodeList.item(i).getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}
			if (!employeesNodeList.item(i).getNodeName().equals("employee")) {
				continue;
			}

			NodeList employeeChildList = employeesNodeList.item(i).getChildNodes();
			employees.add(getEmployee(employeeChildList));
		}
	}

	private static Employee getEmployee(NodeList employeeNodeList) {
		Employee employee = new Employee();

		for (int i = 0; i < employeeNodeList.getLength(); i++) {
			Node employeeNode = employeeNodeList.item(i);

			if (employeeNode.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			switch (employeeNode.getNodeName()) {
				case "name":
					employee.setName(employeeNode.getTextContent());
					break;
				case "surname":
					employee.setSurname(employeeNode.getTextContent());
					break;
				case "email":
					employee.setEmail(employeeNode.getTextContent());
					break;
				case "workingInfo":
					NodeList workingInfos = employeeNode.getChildNodes();
					employee.setWorkingInfo(getWorkingInfo(workingInfos));
					break;
			}
		}

		return employee;
	}

	private static WorkingInfo getWorkingInfo(NodeList workingInfoNodeList) {
		WorkingInfo info = new WorkingInfo();

		for (int i = 0; i < workingInfoNodeList.getLength(); i++) {
			Node workingNode = workingInfoNodeList.item(i);

			if (workingNode.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			switch (workingNode.getNodeName()) {
				case "projectName":
					info.setProjectName(workingNode.getTextContent());
					break;
				case "position":
					info.setPosition(workingNode.getTextContent());
					break;
				case "salary":
					info.setSalary(Integer.parseInt(workingNode.getTextContent()));
					break;
			}
		}

		return info;
	}

	private static Document parseDocument(String path) {
		Document document = null;
		File file = new File(path);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		try {
			document = dbf.newDocumentBuilder().parse(file);
		} catch (IOException | ParserConfigurationException | SAXException e) {
			e.printStackTrace();
			return null;
		}

		return document;
	}
}
