package com.epam.rd.java.basic.task8.model;

public class WorkingInfo {
    private String projectName;
    private String position;
    private Integer salary;

    public WorkingInfo() {

    }

    public WorkingInfo(String projectName, String position, Integer salary) {
        this.projectName = projectName;
        this.position = position;
        this.salary = salary;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "WorkingInfo{" +
                "projectName='" + projectName + '\'' +
                ", position='" + position + '\'' +
                ", salary=" + salary +
                '}';
    }
}
