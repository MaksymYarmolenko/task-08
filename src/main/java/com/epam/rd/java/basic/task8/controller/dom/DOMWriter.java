package com.epam.rd.java.basic.task8.controller.dom;

import com.epam.rd.java.basic.task8.model.Employee;
import com.epam.rd.java.basic.task8.model.WorkingInfo;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class DOMWriter {

    private String path;
    private List<Employee> employees;

    public DOMWriter(String path, List<Employee> employees) {
        this.path = path;
        this.employees = employees;
    }

    public void configureXmlFile() {
        try {
            Document document = getDocument();
            document.appendChild(getRoot(document));

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();

            DOMSource domSource = new DOMSource(document);
            StreamResult streamResult = new StreamResult(new File(path));

            transformer.transform(domSource, streamResult);
        } catch (ParserConfigurationException | TransformerException e) {
            e.printStackTrace();
        }
    }

    private static Document getDocument() throws ParserConfigurationException {
        DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();

        return documentBuilder.newDocument();
    }

    private Element getRoot(Document document) {
        Element root = document.createElement("employees");
        root.setAttribute("xmlns", "http://www.test.ua");
        root.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        root.setAttribute("xsi:schemaLocation", "http://www.test.ua input.xsd");

        for (Employee employee : employees) {
            root.appendChild(getEmployee(document, employee));
        }

        return root;
    }

    private static Element getEmployee(Document document, Employee employee) {
        Element employeeEl = document.createElement("employee");

        Element name = document.createElement("name");
        name.appendChild(document.createTextNode(employee.getName()));
        employeeEl.appendChild(name);

        Element surname = document.createElement("surname");
        surname.appendChild(document.createTextNode(employee.getSurname()));
        employeeEl.appendChild(surname);

        Element email = document.createElement("email");
        email.appendChild(document.createTextNode(employee.getEmail()));
        employeeEl.appendChild(email);

        employeeEl.appendChild(getWorkingInfo(document, employee.getWorkingInfo()));

        return employeeEl;
    }

    private static Element getWorkingInfo(Document document, WorkingInfo workingInfo) {
        Element workingInfoEl = document.createElement("workingInfo");

        Element projectName = document.createElement("projectName");
        projectName.appendChild(document.createTextNode(workingInfo.getProjectName()));
        workingInfoEl.appendChild(projectName);

        Element position = document.createElement("position");
        position.appendChild(document.createTextNode(workingInfo.getPosition()));
        workingInfoEl.appendChild(position);

        Element salary = document.createElement("salary");
        salary.setAttribute("currency", "USD");
        salary.appendChild(document.createTextNode(String.valueOf(workingInfo.getSalary())));
        workingInfoEl.appendChild(salary);

        return workingInfoEl;
    }
}
